# Неделя 27

## Теория

1. Читаем про форму и элементы управления
- https://learn.javascript.ru/forms-controls

Смотрим видео:

- https://www.youtube.com/watch?v=Fgdn-Ub-f2E&ab_channel=WebDev%D1%81%D0%BD%D1%83%D0%BB%D1%8F.%D0%9A%D0%B0%D0%BD%D0%B0%D0%BB%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B0%D0%9B%D1%83%D1%89%D0%B5%D0%BD%D0%BA%D0%BE


## Практика
1. Переделать модальное окно на js
2. Внутри модального окна создаем форму с полями: Имя (input type text), Сообщение (textarea) и кнопка Отправить. По клику на кнопку выводим введенные данные в консоль и закрываем модалку.