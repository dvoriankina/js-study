# Неделя 19

## Теория

**Читаем (и решаем задачи из учебника):**

JS - изучаем классы
- https://learn.javascript.ru/class
- https://learn.javascript.ru/class-inheritance
- https://learn.javascript.ru/static-properties-methods
- https://learn.javascript.ru/private-protected-properties-methods


Смотрим видео
- https://www.youtube.com/watch?v=qjl1nZlW9q8&list=PLM7wFzahDYnHyRpmcSGOptXan08CNb9nh&index=4&ab_channel=WebDev%D1%81%D0%BD%D1%83%D0%BB%D1%8F.%D0%9A%D0%B0%D0%BD%D0%B0%D0%BB%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B0%D0%9B%D1%83%D1%89%D0%B5%D0%BD%D0%BA%D0%BE
- https://www.youtube.com/watch?v=JWwSH92tq7E&list=PLM7wFzahDYnHyRpmcSGOptXan08CNb9nh&index=5&ab_channel=WebDev%D1%81%D0%BD%D1%83%D0%BB%D1%8F.%D0%9A%D0%B0%D0%BD%D0%B0%D0%BB%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B0%D0%9B%D1%83%D1%89%D0%B5%D0%BD%D0%BA%D0%BE


## Практика
- Дорабатываем таймер
1. Приводим в порядок классы
2. Пишем функцию restart
3. Делаем реакцию при наведении на кнопки плавной, используя transition - https://learn.javascript.ru/css-transitions


- Начинаем верстку личной страницы **в отдельном репозитории**

Макет - https://www.figma.com/file/5D9pDuLtS042hzaoN69Kd7/Free--Landing--Page-Template?node-id=0%3A1

Верстаем меню и первые 3 блока: фотография, о себе и скилы. Информацию можно размещать любую. Для скилов можно найти логотипы js/css и т.д.
Скилы выводим из js-файла, там храним объект {название скила - уровень}. Остальную инфу размещаем сразу в html-шаблоне. Верстаем используя flexbox.
Пока что верстаем только десктопную версию, без адаптива. Шрифт сначала берем обычный, Arial.