# Неделя 9

## Практика
Продолжаем работу с основным проектом на Vue3. Делаем компонент дропдауна.

1. Реализуем компонент дропдауна.

- Визульно должен выглядеть так - [1](/guidelines/project/markup/dropdown.png)
- По клику вне дропдауна - закрытие
- Должен закрываться по нажатию на esc
- В пропсах передаем размеры, позиционирование (top, left, right)