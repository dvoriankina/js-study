# Неделя 13

## Теория

**Читаем (и решаем задачи из учебника):**

JS - изучаем setTimeout, setInterval
- https://learn.javascript.ru/settimeout-setinterval

## Практика
- Решаем задачи - https://learn.javascript.ru/settimeout-setinterval#tasks
- Дорабатываем калькулятор