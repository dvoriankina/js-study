# Неделя 4

## Теория

**Читаем (и решаем задачи из учебника):**

Методы строк
- https://learn.javascript.ru/string

JS - if/else, логические операторы
- https://learn.javascript.ru/ifelse
- https://learn.javascript.ru/logical-operators
- https://learn.javascript.ru/nullish-coalescing-operator


## Практика
- Решаем https://www.jschallenger.com/javascript-basics/strings (до замков)
- Решаем https://www.jschallenger.com/javascript-basics/conditionals (до замков)
- Сделать отдельную страницу, на которой выводим строку с любым текстом и 2 кнопки. По нажатию на первую, строка должна стать с заглавной буквы. По нажатию на 2ю - нужно обрезать строку до 30 символов и добавить после них '...'.
- Проходим флексбоксы в верстке - [Флексбоксы](https://flexboxfroggy.com/#ru)
