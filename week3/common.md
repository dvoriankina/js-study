# Неделя 3

## Теория

**Читаем (и решаем задачи из учебника):**

JS - преобразование типов, операторы, сравнение 
- https://learn.javascript.ru/type-conversions
- https://learn.javascript.ru/operators
- https://learn.javascript.ru/comparison


Аттрибуты и свойства, стили и классы, размеры элементов
- https://learn.javascript.ru/dom-attributes-and-properties
- https://learn.javascript.ru/styles-and-classes
- https://learn.javascript.ru/size-and-scroll
 

## Практика
- Решаем https://www.jschallenger.com/javascript-basics/variables
- Решаем https://www.jschallenger.com/javascript-basics/booleans
- Решаем https://www.jschallenger.com/javascript-basics/operators (до замков)
- Вывести внутри рамки с информацией о себе ее размер с паддингом, без паддинга, ее отступ сверху экрана, слева экрана, справа экрана.
