# Неделя 16

## Теория

**Читаем (и решаем задачи из учебника):**

JS - изучаем call/apply, bind, стрелочные ф-ии
- https://learn.javascript.ru/call-apply-decorators
- https://learn.javascript.ru/bind
- https://learn.javascript.ru/arrow-functions

- Смотрим видео про ф-ии, особенно обращаем внимание на стрелочные ф-ии и call, bind, apply: https://www.youtube.com/playlist?list=PLM7wFzahDYnF2VdVjew48HKA7E_pnItDi



## Практика
- Решаем задачи https://learn.javascript.ru/call-apply-decorators#tasks
- https://learn.javascript.ru/bind#tasks
- Дорабатываем таймер:
1. Верстаем как на макете - https://www.figma.com/file/UCZzoWsuXP72mtatZn4xdw/Timer-Animation-(Community)?type=design&node-id=3-12&t=EUjMtTWQmnFmlMOg-0
2. Выносим в переменную время, на которое запускается таймер, что можно было поменять 1 раз в коде