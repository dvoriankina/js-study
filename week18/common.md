# Неделя 18

## Теория

**Читаем (и решаем задачи из учебника):**

Верстка
- Читаем про [бэм-методологию наименования классов](https://ru.bem.info/methodology/key-concepts/)
- Читаем про подход к верстке, принятый в нашей команде - [Базовый подход](../guidelines/markup.md)

JS - изучаем прототипы, наследование
- https://learn.javascript.ru/prototype-inheritance
- https://learn.javascript.ru/function-prototype
- https://learn.javascript.ru/native-prototypes
- https://learn.javascript.ru/prototype-methods

Смотрим видео
- https://www.youtube.com/watch?v=ZmH262CgLsM&list=PLM7wFzahDYnHyRpmcSGOptXan08CNb9nh&index=2&ab_channel=WebDev%D1%81%D0%BD%D1%83%D0%BB%D1%8F.%D0%9A%D0%B0%D0%BD%D0%B0%D0%BB%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B0%D0%9B%D1%83%D1%89%D0%B5%D0%BD%D0%BA%D0%BE
- https://www.youtube.com/watch?v=rWfZAeEnn2I&list=PLM7wFzahDYnHyRpmcSGOptXan08CNb9nh&index=3&ab_channel=WebDev%D1%81%D0%BD%D1%83%D0%BB%D1%8F.%D0%9A%D0%B0%D0%BD%D0%B0%D0%BB%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B0%D0%9B%D1%83%D1%89%D0%B5%D0%BD%D0%BA%D0%BE

## Практика
- Дорабатываем таймер
1. Именуем классы в соответствии с бэм-подходом
2. Вносим правки по верстке
3. Заканчиваем с оживлением функционала