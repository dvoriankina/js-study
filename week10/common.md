# Неделя 10

## Теория

**Читаем (и решаем задачи из учебника):**

JS - продолжаем изучать объекты
- https://learn.javascript.ru/constructor-new
- https://learn.javascript.ru/optional-chaining

JS - изучаем тип данных Symbol, Map, Set
- https://learn.javascript.ru/symbol
- https://learn.javascript.ru/map-set

Верстка - изучаем медиа-запросы
- https://medium.com/nuances-of-programming/%D0%BC%D0%B5%D0%B4%D0%B8%D0%B0-%D0%B7%D0%B0%D0%BF%D1%80%D0%BE%D1%81%D1%8B-css-%D1%82%D0%BE%D1%87%D0%BA%D0%B8-%D0%BE%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%B0-%D1%82%D0%B8%D0%BF%D1%8B-%D1%83%D1%81%D1%82%D1%80%D0%BE%D0%B9%D1%81%D1%82%D0%B2-%D1%81%D1%82%D0%B0%D0%BD%D0%B4%D0%B0%D1%80%D1%82%D0%BD%D1%8B%D0%B5-%D1%80%D0%B0%D0%B7%D1%80%D0%B5%D1%88%D0%B5%D0%BD%D0%B8%D1%8F-%D0%B8-%D0%BC%D0%BD%D0%BE%D0%B3%D0%BE%D0%B5-%D0%B4%D1%80%D1%83%D0%B3%D0%BE%D0%B5-28a49fb66d3d

## Практика
- Решаем задачи по сетам - https://www.jschallenger.com/javascript-practice/javascript-sets

- Дорабатываем калькулятор:
1. Делаем его верстку в соответствии с макетом https://www.figma.com/file/t1TFFEfZPxDGVHkSsDbDiy/Calculator-UI-(Community)?node-id=0-1&t=sCRQzo9F3Bc7dZD1-0
Можно выбрать светлую или темную версию, без разницы. 

Верстка должна быть адаптивной - то есть на каждом размере экрана должна хорошо смотреться. На десктопе размещаем калькулятор по середине экрана, его максимальная ширина должна быть 500px.

Верстку ряда с тремя иконками (часы, линейка, корень) не делаем.

2. Функционал пока делаем ограниченный - должны работать сложение, умножение, деление, равно и сброс (с).