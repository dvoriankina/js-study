# Неделя 26

## Теория

1. Повторяем браузерные события 
- https://learn.javascript.ru/events

Смотрим видео:

- https://www.youtube.com/watch?v=GsYiq4Ic384&list=PLM7wFzahDYnGF4WqYaSuwnItYDEBakTDS&ab_channel=WebDev%D1%81%D0%BD%D1%83%D0%BB%D1%8F.%D0%9A%D0%B0%D0%BD%D0%B0%D0%BB%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B0%D0%9B%D1%83%D1%89%D0%B5%D0%BD%D0%BA%D0%BE


## Практика
1. Делаем появление модального окна по клику на кнопку Отправить сообщение. По клику на крестик или область вне окна оно должно закрываться, по клику по самому окну - нет. Пример - [модалка](modal.png)
2. Вносим правки по обратной связи