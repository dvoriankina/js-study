# Неделя 20

## Теория

**Читаем (и решаем задачи из учебника):**

JS - изучаем колбэки, промисы
- https://learn.javascript.ru/callbacks
- https://learn.javascript.ru/promise-basics
- https://learn.javascript.ru/promise-chaining



Смотрим видео
- https://www.youtube.com/watch?v=c_iQPXDZ_l4&list=PLM7wFzahDYnEltE-aVGhRHYPwIJn0Xquu&index=84&ab_channel=WebDev%D1%81%D0%BD%D1%83%D0%BB%D1%8F.%D0%9A%D0%B0%D0%BD%D0%B0%D0%BB%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B0%D0%9B%D1%83%D1%89%D0%B5%D0%BD%D0%BA%D0%BE

## Практика
- Проверить работоспособность таймера, исправить ошибки

- Продолжаем верстку личной страницы

Макет - https://www.figma.com/file/5D9pDuLtS042hzaoN69Kd7/Free--Landing--Page-Template?node-id=0%3A1

- Приводим внешний вид первых 3х блоков в соответствие с макетом
- Скилы выводим из js-файла, там храним объект {название скила - уровень}. Остальную инфу размещаем сразу в html-шаблоне. Верстаем используя flexbox.
Пока что верстаем только десктопную версию, без адаптива. Шрифт сначала берем обычный, Arial.
- При нажатии на пункт меню делаем плавный скролл к нужному блоку