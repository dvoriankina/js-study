# Неделя 5

## Теория

**Читаем (и решаем задачи из учебника):**
while/for, switch
- https://learn.javascript.ru/while-for
- https://learn.javascript.ru/switch

Браузерные события
- https://learn.javascript.ru/events
- https://learn.javascript.ru/event-details


## Практика
- Решаем https://www.jschallenger.com/javascript-dom-exercises/events-and-user-interactions (до замков)
- Решаем https://www.jschallenger.com/javascript-dom-exercises/dom-manipulation (до замков)
- Решаем https://www.jschallenger.com/javascript-practice/javascript-fundamentals (до замков)
- На странице сделать вывод координат курсора при его передвижении (x, y)