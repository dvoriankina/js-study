# Неделя 12

## Теория

**Читаем (и решаем задачи из учебника):**

JS - изучаем spread-оператор, замыкание
- https://learn.javascript.ru/rest-parameters-spread-operator
- https://learn.javascript.ru/closure

## Практика
1. Решаем задачи https://learn.javascript.ru/closure#tasks

Для задач надо написать код:

- https://learn.javascript.ru/closure#summa-s-pomoschyu-zamykaniy
- https://learn.javascript.ru/closure#filtratsiya-s-pomoschyu-funktsii
- https://learn.javascript.ru/closure#sortirovat-po-polyu

2. Дорабатываем калькулятор