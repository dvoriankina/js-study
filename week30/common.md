# Неделя 30

## Теория

1. Еще раз освежаем теорию по верстке, если есть необходимость

- https://learn.javascript.ru/css-for-js
- https://www.youtube.com/watch?v=BmaUFltuyWY&list=PLM7wFzahDYnHKFV3AF8dtl6pBYgib1mMf&ab_channel=WebDev%D1%81%D0%BD%D1%83%D0%BB%D1%8F.%D0%9A%D0%B0%D0%BD%D0%B0%D0%BB%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B0%D0%9B%D1%83%D1%89%D0%B5%D0%BD%D0%BA%D0%BE
- https://code-basics.com/ru/languages/css


## Практика
Начинаем верстку сайта по реальным макетам:

1. Верстаем главную страницу https://www.figma.com/proto/Eau2LybsThsA1kQ6YlRDYY/Untitled?page-id=45%3A38&node-id=114-187&viewport=-3750%2C-528%2C0.28&scaling=min-zoom&starting-point-node-id=114%3A187&hide-ui=1

Картинки для страницы можно взять [тут](../guidelines/vuz/01-home.zip)