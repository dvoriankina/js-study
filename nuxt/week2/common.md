# Nuxt. Неделя 2

## Практика
1. Реализовать api /api/tips, которое будет отдавать следующую структуру данных - [данные](/guidelines/project/nuxt/tips-data.png)
2. Сверстать главную страницу - [раз](/guidelines/project/nuxt/1.png)
3. При вводе в поисковый инпут символов должен уходить запрос на api/tips и появляться данные в подсказках к поиску - [пример](/guidelines/project/nuxt/tips.png)