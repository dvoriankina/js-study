# Неделя 28

## Теория

1. Читаем про загрузку страницы
- https://learn.javascript.ru/loading


## Практика
1. Дорабатываем форму:
- Поля располагаем вертикально друг под другом
- У каждого поля должен быть label и placeholder
- По сабмиту формы вызываем ф-ю, которая выводит в консоль данные и закрывает модалку.

2. В форму добавляем селект "Должность": Студент, Специалист
3. В форму добавляем поле для ввода email. Получается, должны быть поля: Имя, Email, Должность, Сообщение
4. Стилизуем инпуты и селект, для кнопки используем тот же класс, который уже есть в проекте для кнопки. Можно посмотреть, как тут происходит https://www.w3schools.com/css/css_form.asp