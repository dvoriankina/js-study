# Неделя 8

## Теория

**Читаем (и решаем задачи из учебника):**

JS - начинаем погружаться в объекты
- https://learn.javascript.ru/object
- https://learn.javascript.ru/object-copy

- Заканчиваем курс по гиту

## Практика
- Решаем задачи https://www.jschallenger.com/javascript-practice/javascript-objects
- Решаем задачи и выгружаем решение в свой репозиторий:

1. isPlainObject. Напишите функцию, которая проверяет, является ли элемент именно простым объектом, а не массивом, null и т.п.
```
const data = { a: 1 };
console.log(isPlainObject(data)); // true
```
2. IsEqual. Напишите функцию, которая поверхностно сравнивает два объекта.
```
const data = { a: 1, b: 1 };
const data2 = { a: 1, b: 1 };
const data3 = { a: 1, b: 2 };
console.log(isEqual(data, data2)); // true
console.log(isEqual(data, data3)); // false
```
3. MakePairs. Напишите функцию, которая возвращает вложенный массив вида [[key, value], [key, value]].
```
const data = { a: 1, b: 2 };
console.log(makePairs(data)); // [['a', 1], ['b', 2]]
```
4. MultiplyNumeric. Создайте функцию multiplyNumeric(obj, num), которая умножает все числовые свойства объекта obj на переданный num и возвращает новый объект.
```
let menu = {
  width: 200,
  height: 300,
  title: "My menu"
};

multiplyNumeric(menu);

menu = {
  width: 400,
  height: 600,
  title: "My menu"
};
```
