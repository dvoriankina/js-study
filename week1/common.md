# Неделя 1

## Теория

**Читаем:**

Знакомство с JS, знакомство с DOM
- https://learn.javascript.ru/getting-started
- https://learn.javascript.ru/hello-world
- https://learn.javascript.ru/structure
- https://learn.javascript.ru/strict-mode
- https://learn.javascript.ru/browser-environment
- https://learn.javascript.ru/dom-nodes


## Практика
1. Пройти (надо зарегистрироваться)
- https://htmlacademy.ru/courses/299
- https://htmlacademy.ru/courses/301


2. Сделать html-страницу и подключить туда файлы со стилями и скриптом. В скрипте написать код, который в консоли выведет информацию о своем юзер-агенте и урл страницы.


## Внеклассное чтение
[Код. Тайный язык информатики](../books/Ch_Pettsold_-_Kod_Tayny_yazyk_informatiki.pdf)