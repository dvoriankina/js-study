# Неделя 2


## Теория

**Читаем:**

JS - переменные, типы данных
- https://learn.javascript.ru/variables
- https://learn.javascript.ru/types

Рендеринг страницы, загрузка скриптов
- https://developer.mozilla.org/ru/docs/Web/Performance/Critical_rendering_path
- https://learn.javascript.ru/loading

DOM-дерево, навигация по дом-элементам
- https://learn.javascript.ru/dom-navigation
- https://learn.javascript.ru/searching-elements-dom
- https://learn.javascript.ru/basic-dom-node-properties
- https://learn.javascript.ru/modifying-document
 

## Практика
1. Пройти
- https://htmlacademy.ru/courses/305
- https://htmlacademy.ru/courses/307

2. Решаем задачи https://www.jschallenger.com/javascript-dom-exercises/dom-selector-methods (4 задачи внутри этого блока)
3. Сделать вывод информации о себе на странице из скрипта. В скрипте создать объект user с полями: name, birth, pic (любая картинка/фотка), profession, userAgent, skills (массив скиллов). Информацию располагаем по центру страницы в рамке (border любого цвета и ширины по желанию).
4. Через скрипт изменить цвет рамки
5. Поэксперементровать со скриптом на своей странице - добавить его в начало, в конец перед `</body>`, навесить async, навесить defer, посмотреть, что будет

## Внеклассное чтение 
[Код. Тайный язык информатики](../books/Ch_Pettsold_-_Kod_Tayny_yazyk_informatiki.pdf)